#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS: 
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
clear
echo "-----------------------------"
echo "             Menu            "
echo "-----------------------------"
echo "(1) Restart"
echo "(2) Change Keyboard Language"
echo "(3) Shutdown"
echo "(4) Change Screen Position"
echo "(0) Exit"
echo "-----------------------------"
read -p "Your choice is: " choice
echo "-----------------------------"

if [ $choice = 1 ]
  then
  read -p "Are you Sure? (y/n)" confirmation
  echo "-----------------------------" 
 if [ $confirmation = "y" ]
    then
    sh ./restart.sh
  else
    echo "Operation Canceled"
    sh ./ms1-menu.sh
  fi
elif [ $choice = 2 ]
  then
  read -p "Enter keyboard language code: " language
  echo "-----------------------------"
  sh ./keyboard-language.sh $language
  echo "-----------------------------"
  read -p "Press enter to continue"
  sh ./ms1-menu.sh
elif [ $choice = 3 ]
  then 
  sh ./shutdown.sh
  sh ./ms1-menu.sh
elif [ $choice = 4 ]
  then
  read -p "Enter the screen direction that you want (right/left): " direction
  echo "-----------------------------"
  if [ $direction = "left" ]
    then
    sh ./turn-screen.sh left
    sh ./ms1-menu.sh
  elif [ $direction = "right" ]
    then
    sh ./turn-screen.sh right
    sh ./ms1-menu.sh
  else
    echo "Invalid format"
    sh ./ms1-menu.sh
   fi
elif [ $choice = 0 ]
  then
  echo "Have a good Day!!"
  echo "       Bye       "
  exit 0
else
  echo "Invalid format"
  sh ./ms1-menu.sh
fi

#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
